# Forge Wheel of Fortune 

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This is a simple example which generates a spinning wheel from the contents of a table on the page and selects a random row.  

![Example gif](./docs/images/wheel.gif)

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app

## Documentation

The app's [manifest.yml](./manifest.yml) contains two modules:

1. A [macro module](https://developer.atlassian.com/platform/forge/manifest-reference/#macro) that specifies the metadata displayed to the user in the Confluence editor's quick insert menu.
2. A corresponding [function module](https://developer.atlassian.com/platform/forge/manifest-reference/#function) that implements the macro logic.

The function logic is implemented wholly in a single file [src/index.tsx](./src/index.tsx).
 
The app's UI is implemented using these features:

- One [Editor Macro](https://developer.atlassian.com/platform/forge/manifest-reference/#macro) that displays a spinning wheel visualization.

- A [Button](https://developer.atlassian.com/platform/forge/ui-components/button/) to trigger the wheel to spin.

- [Image](https://developer.atlassian.com/platform/forge/ui-components/image) component.

- [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) image format to dynamically generate an image. 

- [useProductContext](https://developer.atlassian.com/platform/forge/ui-hooks-reference/) to get the id of the current page.  

- [useAction](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#useaction) hooks.

- [api.asUser().requestConfluence](https://developer.atlassian.com/platform/forge/runtime-api-reference/#requestconfluence) to read the contents of the current page.

## Contributions

Contributions to Forge Wheel of Fortune are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.