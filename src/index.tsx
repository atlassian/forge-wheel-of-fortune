import ForgeUI, { render, Fragment, Text , Image, useProductContext, useState, Button, useAction} from "@forge/ui";
import { traverse, } from '@atlaskit/adf-utils/traverse.es'; // .es for ES2015
import api, {route} from "@forge/api";

// import { colorPalettes } from '@atlaskit/theme'; TODO - switch to using Atlassian colors

// TODOs:
// better list of colors
// show the winner above/ below the wheel
// weighting for the different segment


const colors = [
   /*  aqua: */ "#00ffff",
   /*  beige:  */"#f5f5dc",
    // /* black:  */"#000000",
    /* blue: */ "#0000ff",
   /*  brown: */ "#a52a2a",
    /* darkblue:  */"#00008b",
    /* darkcyan: */ "#008b8b",
   /*  darkgrey: */ "#a9a9a9",
    /* darkgreen:  */"#006400",
   /*  darkkhaki: */ "#bdb76b",
   /*  darkmagenta:  */"#8b008b",
   /*  darkolivegreen:  */"#556b2f",
    /* darkorange: */ "#ff8c00",
    /* darkorchid: */ "#9932cc",
    /* darkred:  */"#8b0000",
    /* darksalmon: */ "#e9967a",
    /* darkviolet:  */"#9400d3",
    /* fuchsia: */ "#ff00ff",
    /* gold: */ "#ffd700",
    /* green:  */"#008000",
    /* indigo:  */"#4b0082",
   /*  khaki: */ "#f0e68c",
   /*  lightblue:  */"#add8e6",
    /* lightcyan: */ "#e0ffff",
    /* lightgreen:  */"#90ee90",
    /* lightgrey:  */"#d3d3d3",
    /* lightpink:  */"#ffb6c1",
    /* lightyellow:  */"#ffffe0",
    /* lime:  */"#00ff00",
    /* magenta:  */"#ff00ff",
    /* maroon: */ "#800000",
    /* navy: */ "#000080",
   /*  olive: */ "#808000",
    /* orange:  */"#ffa500",
    /* pink:  */"#ffc0cb",
    /* purple:  */"#800080",
    // /* violet:  */"#800080", Duplicate of above
    /* red:  */"#ff0000",
    /* silver: */ "#c0c0c0",
    /* white: */ "#ffffff",
   /*  yellow: */ "#ffff00",
       /* azure: */ "#f0ffff",
  /* cyan:  */"#00ffff",

];

  function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
  
    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians))
    };
  }
  
  function describeArc(x, y, radius, startAngle, endAngle){
  
      var start = polarToCartesian(x, y, radius, endAngle);
      var end = polarToCartesian(x, y, radius, startAngle);
  
      var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";
  
      var d = [
          "M", start.x, start.y, 
          "A", radius, radius, 0, arcSweep, 0, end.x, end.y,
          "L", x,y,
          "L", start.x, start.y
      ].join(" ");
  
      return d;       
  }
  const getSegmentNames = async () => {
    const context = useProductContext();

    const contentId = context.contentId
    
    const result = await api
    .asUser()
    .requestConfluence(route`/wiki/rest/api/content/${contentId}?expand=body.view,body.editor,body.editor2,body.atlas_doc_format`)
    const data = await result.json();
    // console.log(await result.status)
    // console.log(data)
    const adfDoc = JSON.parse(data.body.atlas_doc_format.value);
    var segmentNames = []
    // console.log(adfDoc)
    traverse(adfDoc, {
      table: (node, parent) => {
        console.log("Found table "+ JSON.stringify(node, null, 2))
        // segmentNames = Array(node.content.length)
        for(var i=0;i<node.content.length;i++){
          // console.log(JSON.stringify(node.content[i].content[0]))
          if (node.content[i].content[0].type === 'tableHeader'){
            // console.debug("Skipping table heading")
          } else {
            const row = node.content[i];
            const cell = row.content[0];
            const paragraph = cell.content[0];
            if (paragraph && paragraph.type === 'paragraph' && paragraph.content) {
              const paragraphText = paragraph.content[0];
              if (paragraphText && paragraphText.type === 'text' && paragraphText.text) {
                var name = paragraphText.text.trim();
                if (name) {
                  name = name.replace('&',' ').substring(0,30);
                  segmentNames.push(name)
                }
              }
            }
          }
        }

      },
      extension: (node, parent) => {
        // console.log("node node"+ JSON.stringify(node .attrs.parameters)+ "******" + context.localId)
        
        try {
          if (node.attrs.parameters.localId === context.localId) {
            // console.log("node "+ JSON.stringify(node) )
            // console.log("parent"+ JSON.stringify(parent.node.content))
            return node;
          }
        } catch (err) {
          console.log("Error on ", node)
          return node;
        }


      }

    });
    // console.log("traverse done"+ segmentNames)

    // console.log('Returning segmentNames', segmentNames);
    return segmentNames
  }

  const getSegments =  async () => {
    const segmentNames = await getSegmentNames()
    // var colors = ["red","blue","green","yellow","black","grey","white"]
    var svgPaths = ''
    var i: number
    for ( i=0;i < segmentNames.length;i++){

        //  console.debug("segment:"+segmentNames[i] +i+" ----"+ i*360/segmentNames.length+","+(i+1)*360/segmentNames.length+"<"+(i+0.5)*360/segmentNames.length)
      svgPaths +='<path d= "'+ describeArc(600,600,500,i*360/segmentNames.length,(i+1)*360/segmentNames.length) + ' " fill="'+colors[i %40].replace(/#/g, '%23') +'" fill-opacity="0.4" />  \n'
      const textAngle = (i+0.3)*360/segmentNames.length + 90 
      var fontSize = 20
      var spacer = "                         "
      if (segmentNames.length < 50){
        fontSize = 20 + 30 * ( 50 - segmentNames.length)/50
        spacer ="      "
        for (let j = 0 ; j<segmentNames.length-25;j++){
          spacer += " "
        }
      }
      svgPaths +=`<text transform="rotate(`+ textAngle+` 600,600)" style="text-anchor:end;" x="600" y="600" fill="black" font-size="`+fontSize+`" font-weight="bold" font-family="Arial" xml:space="preserve">`+segmentNames[i]+spacer +`</text> \n `
      // svgPaths +=`<text transform="rotate(90,600,600)" style="text-anchor:end;" x="600" y="600" fill="black" font-size="25" font-weight="bold" font-family="Arial">`+segmentNames[i]+`</text>`
    }
    // console.debug(svgPaths)
    return svgPaths
  }  

  // speed is 1s @ 6x + 1s @2s + 1s @ 1x - total 9x 
  const  generateSVG = async (spinning)=> {
    const segments = await getSegments();
    const spinToAngle = 360+(Math.random()*360)
    var animate 
    if (spinning) {
       animate =  ` <animateTransform
    attributeName="transform"
    type="rotate"
    from="0 600 600"
    to="`+spinToAngle/9*6+` 600 600"
    begin="0s"
    dur="2s"
    repeatCount="1"
    fill="freeze"/>`
    animate +=  ` <animateTransform
    attributeName="transform"
    type="rotate"
    from="`+spinToAngle/9*6+` 600 600"
    to="`+spinToAngle/9*8+` 600 600"
    begin="2s"
    dur="2s"
    repeatCount="1"
    fill="freeze"/>`
    animate +=  ` <animateTransform
    attributeName="transform"
    type="rotate"
    from="`+spinToAngle/9*8+` 600 600"
    to="`+spinToAngle+` 600 600"
    begin="4s"
    dur="2s"
    repeatCount="1"
    fill="freeze"/>`

  }else {
       animate = ''
    }

    return   'data:image/svg+xml;utf8,' + `<svg xmlns="http://www.w3.org/2000/svg" height="1200" width="1200" viewBox="0 0 1200 1200">` +  
    '<g >'+
      segments + 
      animate+
  `</g> 

  <g><polygon points="640,40  600,120 560,40  " fill="black" /></g>
  </svg>`
  }




const App = () => {  

  const [svgUrl, spin] = useAction(()=>generateSVG(true) ,()=>generateSVG(false));

  console.info(svgUrl)
 
  return (
    <Fragment>
      <Image  src={svgUrl}  alt="Wheel"/>
      <Button  text="spin" 
        onClick={() => spin()}
        />


    </Fragment>
  );
};

export const run = render(<App />);
